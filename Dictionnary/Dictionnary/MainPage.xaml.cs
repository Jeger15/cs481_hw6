﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Net.Http;
using System.Net.Http.Headers;


/* Namespace & class MainPage for Backend & creation of the dictionary part */
// Utilisation of https://docs.microsoft.com/en-us/xamarin/xamarin-forms/data-cloud/web-services/rest for the API

namespace Dictionnary
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        // Xamarin Tools used to send and receive requests over HTTP.
        HttpClient client;
        // URL For Request 
        string API_URL = "https://owlbot.info/api/v4/dictionary/";
        // Token for request the OwlBot API
        string token = "67ba8c95f3d78a8e0eb53d7c49ac45c1222439b9";

        // Initialization of the tools
        public MainPage()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", token);
            InitializeComponent();
        }

        //Search Function link to the button
        public async void SearchWord(object sender, EventArgs args)
        {
            // URL with the word for the request
            var New_URL = new Uri(string.Format(this.API_URL + this.Input.Text, string.Empty));
            // VAR for check the internet access
            var Check_Internet = Connectivity.NetworkAccess;

            if (Check_Internet == NetworkAccess.Internet)
            {
                // Send the request GET & Wait for the response
                var response_request = await client.GetAsync(New_URL);

                if (response_request.IsSuccessStatusCode)
                {
                    // Parsing the request content
                    var content = await response_request.Content.ReadAsStringAsync();

                    try {
                        // If the request is good & the result too
                        this.Stack_Base.Children.Add(new Label {Text = content });
                        Console.WriteLine(content);
                    }

                    //If the request is good & the display of the result is not good.
                    catch {
                        await DisplayAlert("Error", "The result cannot be displayed.", "OK");
                    }
                }
                else
                    await DisplayAlert("Error", response_request.ReasonPhrase, "OK");
            }
            else
                await DisplayAlert("Error", "The Access of Internet isn't available, try again.", "OK");
        }

    }
}
